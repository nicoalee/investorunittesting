package com.citi.InvestorActual;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.function.Consumer;

import org.junit.Test;

import com.citi.trading.Investor;
import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;


/**
 * Unit test for simple App.
 */
public class InvestorTest {
	
	public class MockMarket implements OrderPlacer {

		public Consumer<Trade> callback;
		public Trade trade;
		
		@Override
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			
			String tradeType = "";
			
			if(order.isBuy()) {
				tradeType = "bought";
			} else {
				tradeType = "sold";
			}
			
//			System.out.print("Investor " + tradeType + " " + order.getStock() + 
//							 " of size " + order.getSize() + 
//							 " costing " + order.getPrice());
			this.callback = callback;
			this.trade = order;
		}
	}
	
	@Test
	public void testInvestorCash() {
		
		int cash = 10000;
		
		MockMarket market = new MockMarket();
		Investor investor = new Investor(cash, market);				

		investor.buy("MRK", 10, 10);
		market.callback.accept(market.trade);
		assertThat (investor.getCash(), closeTo(9900, 0.0001));

		investor.sell("MRK", 10, 10);
		market.callback.accept(market.trade);
		assertThat (investor.getCash(), closeTo(10000, 0.0001));
	}
	
	@Test
	public void testCallBack() {
		
		int cash = 10000;
		
		MockMarket market = new MockMarket();
		Investor investor = new Investor(cash, market);	
		
		investor.buy("GOOGL", 100, 9);
		assertThat (investor.getPortfolio().size(), is(0));
		assertThat (investor.getCash(), closeTo(cash, 0.0001));
	}
	
	@Test
	public void testPortfolio() {
		
		int cash = 10000;
		
		MockMarket market = new MockMarket();
		Investor investor = new Investor(cash, market);	
		
		investor.buy("AAPL", 100, 10);
		market.callback.accept(market.trade);
		
		investor.buy("GOOGL", 20, 4);
		market.callback.accept(market.trade);
		
		investor.buy("C", 20, 2);
		market.callback.accept(market.trade);
		
		assertThat (investor.getPortfolio().size(), is(3));
		assertThat (investor.getPortfolio().containsKey("AAPL"), is(true));
		assertThat (investor.getPortfolio().containsKey("GOOGL"), is(true));
		assertThat (investor.getPortfolio().containsKey("C"), is(true));
	}
	
	// The following tests will drive development, and test the constraints of the input to investor
	
	@Test(expected=IllegalArgumentException.class)
	public void testSellWithoutOwningStock() {
		
		int cash = 10000;
		
		MockMarket market = new MockMarket();
		Investor investor = new Investor(cash, market);	
		
		investor.sell("GOOGL", 100, 10);
		market.callback.accept(market.trade);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSellWithoutOwningEnough() {
		
		int cash = 10000;
		
		MockMarket market = new MockMarket();
		Investor investor = new Investor(cash, market);	
		
		investor.buy("AAPL", 90, 10);
		market.callback.accept(market.trade);

		investor.sell("AAPL", 100, 10);
		market.callback.accept(market.trade);
		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPurchaseWithoutEnoughFunds() {
		
		int cash = 1;
		
		MockMarket market = new MockMarket();
		Investor investor = new Investor(cash, market);	
		investor.buy("C", 1, 2);
		market.callback.accept(market.trade);
		
	}
	
	@Test
	public void portfolioRemoval() {
		
		int cash = 10000;
		
		MockMarket market = new MockMarket();
		Investor investor = new Investor(cash, market);	
		
		investor.buy("CRRFY", 1000, 4);
		market.callback.accept(market.trade);
		investor.sell("CRRFY", 1000, 4);
		market.callback.accept(market.trade);
		
		assertThat (investor.getPortfolio().size(), is(0));
	}
	
}
