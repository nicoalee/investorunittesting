package com.citi.trading;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * Encapsulates a stock investor with a portfolio of stocks, some cash,
 * and the ability to place trade orders.
 * 
 * @author Will Provost
 */
public class Investor {

	private Map<String,Integer> portfolio;
	private double cash;
	private OrderPlacer market;

	/**
	 * Handler for trade confirmations.
	 */
	private class NotificationHandler implements Consumer<Trade> {
		public void accept(Trade trade) {
			synchronized(Investor.this) {
				String stock = trade.getStock();
				if (trade.isBuy()) {
					
					int costOfStocks = (int) ((int) trade.getSize() * trade.getPrice());
					if(cash >= costOfStocks) {
						
						if (!portfolio.containsKey(stock)) {
							portfolio.put(stock, 0);
						}
						portfolio.put(stock, portfolio.get(stock) + trade.getSize());
						cash -= trade.getPrice() * trade.getSize();
						
					} else {
						
						throw new IllegalArgumentException();
						
					}
					

				} else {
					
					if(portfolio.containsKey(stock)) {
						
						int numStocks = portfolio.get(stock);
						
						if(numStocks >= trade.getSize()) {
							
							int numStocksAfterSale = portfolio.get(stock) - trade.getSize();
							
							if(numStocksAfterSale == 0) {
								
								portfolio.remove(stock);
								
							} else {
								
								portfolio.put(stock, numStocksAfterSale);
								
							}
						
							cash += trade.getPrice() * trade.getSize();
							
						} else {
							throw new IllegalArgumentException();
						}
						
					} else {
						throw new IllegalArgumentException();
						
					}
				}
			}
		}
	}
	private NotificationHandler handler = new NotificationHandler();
	
	/**
	 * Create an investor with some cash but no holdings.
	 */
	public Investor(double cash, OrderPlacer market) {
		this(new HashMap<>(), cash, market);
	}
	
	/**
	 * Create an investor with holdings as a map of tickers and share counts,
	 * and some cash on hand.
	 */
	public Investor(Map<String,Integer> portfolio, double cash, OrderPlacer market) {
		for (int shares : portfolio.values()) {
			if (shares <= 0) {
				throw new IllegalArgumentException("All share counts must be positive.");
			}
		}
		
		this.portfolio = portfolio;
		this.cash = cash;
		this.market = market;
		
	}
	
	/**
	 * Accessor for the portfolio.
	 */
	public Map<String,Integer> getPortfolio() {
		return portfolio;
	}
	
	/**
	 * Accessor for current cash. 
	 */
	public double getCash() {
		return cash;
	}
	
	/**
	 * Places an order to buy the given stock.
	 */
	public synchronized void buy(String stock, int size, double price) {
		Trade trade = new Trade(stock, true, size, price);
		market.placeOrder(trade, handler);
	}
	
	/**
	 * Places an order to sell the given stock.
	 */
	public synchronized void sell(String stock, int size, double price) {
		Trade trade = new Trade(stock, false, size, price);
		market.placeOrder(trade, handler);
	}
	
	/**
	 * Quick test of the Investor class.
	 */
	public static void main(String[] args) {
		
		int cash = 10000;
		
		Investor investor = new Investor(cash, new Market());
		investor.buy("MRK", 100, 60);
		System.out.println("Placed order ...");
		try {
			Thread.sleep(15000);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
		
		System.exit(0);
	}
}
